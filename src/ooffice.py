#!/usr/bin/env python
#
# document.py: front end to OpenOffice documents opening and searching
#
#
# Copyright (C) 2005 Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
import sys
import os.path
import errno
import zipfile
import libxml2
import urlparse
import util
import document
import docmanui
import indexing
import StringIO

class OODocument(document.Document):
    """Class abstracting a searchable document"""
    def __init__(self, URL):
        """The __init__ method should raise a Document exception id the
	   document is not of the expected type"""
	self.URL = URL
	self.path = None
	self.__index = None
	try:
	    decomp = urlparse.urlparse(URL)
	    if decomp[2] == URL:
	        self.path = URL
	except:
	    pass
	if self.path == None:
	    raise document.DocumentException("Web indexing not yet supported")
	
	try:
	    self.zip = zipfile.ZipFile (self.path, "r")
	except:
	    raise document.DocumentException("Not an OpenOffice document")

        try:
	    self.mimetype = self.zip.read("mimetype")
	except:
	    raise document.DocumentException("Not an OpenOffice document")
        
	self.__getMetadata()
	
    def __getMetadata(self):
        """Extract the metadata portion of the container and refresh the
	 informations about it"""
        try:
            blob = self.zip.read("meta.xml")
            doc = libxml2.readMemory (blob, len (blob),
                                      "meta.xml",
                                      None,
                                      libxml2.XML_PARSE_NOBLANKS)
        except:
	    import traceback
	    import sys
	    traceback.print_exc(file=sys.stderr)
	    raise document.DocumentException("Metadata for %s are missing or corrupted" % self.URL)
	ctxt = doc.xpathNewContext()
	ctxt.xpathRegisterNs("office", "http://openoffice.org/2000/office")
	ctxt.xpathRegisterNs("dc", "http://purl.org/dc/elements/1.1/")
	ctxt.xpathRegisterNs("meta", "http://openoffice.org/2000/meta")

	self.__creator = ctxt.xpathEval("string(/office:document-meta/office:meta/meta:initial-creator)")
	last_author = ctxt.xpathEval("string(/office:document-meta/office:meta/dc:creator)")
	if last_author != self.__creator:
	    self.__authors = (last_author, self.__creator)
	else:
	    self.__authors = (last_author)
	self.__creation_date = ctxt.xpathEval("string(/office:document-meta/office:meta/meta:creation-date)")
	self.__last_modified = ctxt.xpathEval("string(/office:document-meta/office:meta/dc:date)")

	ctxt.xpathFreeContext()
	doc.freeDoc()

    def documentType(self):
        """A string identifying the document type"""
	if self.mimetype == "application/vnd.sun.xml.draw":
	    return _("OpenOffice Drawing")
	if self.mimetype == "application/vnd.sun.xml.writer":
	    return _("OpenOffice Document")
	if self.mimetype == "application/vnd.sun.xml.calc":
	    return _("OpenOffice Sheet")
	if self.mimetype == "application/vnd.sun.xml.impress":
	    return _("OpenOffice Presentation")
	return self.mimetype

    def creationDate(self):
        """Date the document was created"""
	return self.__creation_date

    def creator(self):
        """who created the docuemnt"""
	return self.__creator

    def authors(self):
        """the list of all author names"""
	return self.__authors

    def lastModification(self):
        """date of last modification"""
	return self.__last_modified

    def reindex(self):
        """(re)build the index for the document. Returns the number of
	   words indexed or -1 in case of error."""

        statusPush(_("Indexing %s"), self.path)
        self.__index = {}
        if self.zip == None:
	    try:
		self.zip = zipfile.ZipFile (self.path, "r")
	    except:
	        util.print_exception()
		statusPop()
	        return -1

	try:
	    content = self.zip.read("content.xml")
	    input = libxml2.inputBuffer(StringIO.StringIO(content))
	    reader = input.newTextReader(self.path)
	except:
	    util.print_exception()
	    statusPop()
	    return -1

	while reader.Read() == 1:
	    if reader.NodeType() == libxml2.XML_READER_TYPE_TEXT:
                indexing.indexString(self.__index, reader.Value())
	        
	statusPop()
	return len(self.__index.keys())

    def index(self):
        """return a weighted hash of the words in the documents."""
	if self.__index == None:
	    self.reindex()
	return self.__index
    
document.addDocumentType((".sxc", ".sxi", ".sxd", ".sxw"),
                         ("ooffice", "staroffice"),
                         OODocument)

def test_file(filename):
    try:
        doc = OODocument(filename)
	l = doc.reindex()
	print "indexed %s words" % (l)
    except:
        print "Failed to open and query %s" % (filename)
	import traceback
	import sys
	traceback.print_exc(file=sys.stderr)
	return

    print "%s: %s from %s at %s" % (filename, doc.documentType(), doc.authors(), doc.lastModification())

statusid=None
def statusPop():
    if statusid == None:
        return
    docmanui.statusPop(statusid)

def statusPush(fmt, *args):
    if statusid == None:
        docmanui.statusGet("oofice")
    if statusid == None:
        return
    docmanui.statusPush(statusid, fmt % args)

    
def run_unit_tests():
    test_file("FudCon2Speeches.sxc")

if __name__ == "__main__":
    run_unit_tests()


