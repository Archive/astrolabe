#!/usr/bin/env python

#
# Copyright (C) 2005 Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
import os
import util
import os.path
import libxml2

filename = os.path.join (util.get_home_dir(), ".astrolabe")
doc = None

# don't report errors from libxml2 parsing
def libxml2_no_error_callback(ctx, str):
    pass

libxml2.registerErrorHandler(libxml2_no_error_callback, "")

def dprint(fmt, *args):
    util.debug_print (util.DEBUG_CONFIG, fmt % args)

def load_config(dialog, model, store, dirscanner = None):
    global doc
    if doc == None:
	try:
	    doc = libxml2.readFile(filename, None, 0)
	except:
	    dprint("Failed to parse config file %s", filename)
    if doc == None:
        return

    try:
	if dialog != None:
	    dialog.load_config(doc)
    except:
        pass
    try:
	if model != None:
	    model.load_config(doc)
    except:
        pass
    try:
	if store != None:
	    store.load_config(doc)
    except:
        pass
    try:
	if dirscanner != None:
	    dirscanner.load_config(doc)
    except:
        pass

def save_config(dialog, model, store, dirscanner = None):
    dprint("Saving config")
    try:
	fd = open(filename, "w")
    except:
        dprint("Failed to write to config file %s", filename)
    fd.write("<astrolabe>\n")
    try:
	if dialog != None:
	    dialog.save_config(fd)
    except:
        pass
    try:
	if model != None:
	    model.save_config(fd)
    except:
        pass
    try:
	if store != None:
	    store.save_config(fd)
    except:
        pass
    try:
	if dirscanner != None:
	    dirscanner.save_config(fd)
    except:
        pass
    fd.write("</astrolabe>\n")
    fd.close()

try:
    dprint("Loading config")
    doc = libxml2.readFile(filename, None, 0)
except:
    dprint("Failed to parse config file %s", filename)

def get_config():
    global doc

    if doc == None:
	try:
	    doc = libxml2.readFile(filename, None, 0)
	except:
	    dprint("Failed to parse config file %s", filename)

    return doc
