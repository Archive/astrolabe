#!/usr/bin/env python
#
# document.py: front end to OpenOffice documents opening and searching
#
#
# Copyright (C) 2005 Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
import sys
import util

dropWords = {
    'the':0, 'this':0, 'can':0, 'man':0, 'had':0, 'him':0, 'only':0,
    'and':0, 'not':0, 'been':0, 'other':0, 'even':0, 'are':0, 'was':0,
    'new':0, 'most':0, 'but':0, 'when':0, 'some':0, 'made':0, 'from':0,
    'who':0, 'could':0, 'after':0, 'that':0, 'will':0, 'time':0, 'also':0,
    'have':0, 'more':0, 'these':0, 'did':0, 'was':0, 'two':0, 'many':0,
    'they':0, 'may':0, 'before':0, 'for':0, 'which':0, 'out':0, 'then':0,
    'must':0, 'one':0, 'through':0, 'with':0, 'you':0, 'said':0,
    'first':0, 'back':0, 'were':0, 'what':0, 'any':0, 'years':0, 'his':0,
    'her':0, 'where':0, 'all':0, 'its':0, 'now':0, 'much':0, 'she':0,
    'about':0, 'such':0, 'your':0, 'there':0, 'into':0, 'like':0, 'may':0,
    'would':0, 'than':0, 'our':0, 'well':0, 'their':0, 'them':0, 'over':0,
    'down':0, 'net':0, 'bad':0, 'Okay':0, 'bin':0, 'cur':0,
}

#
# dropChar = (".", "!", "?", ",", "'", '"', ";", "(", ")", "{", "}", "<", 
#             ">", "=", "/", "*", ":", "#", "\\", "\n", "\r")
#
# LOWER: everything is translated to lowercase for easier search, but
#        admitedly it is controversial
#
translateLowerChar = ""
translateChar = ""
for i in range(256):
    if i == 46:
        translateChar = translateChar + " "
        translateLowerChar = translateLowerChar + " "
    elif i == 33:
        translateChar = translateChar + " "
        translateLowerChar = translateLowerChar + " "
    elif i == 63:
        translateChar = translateChar + " "
        translateLowerChar = translateLowerChar + " "
    elif i == 44:
        translateChar = translateChar + " "
        translateLowerChar = translateLowerChar + " "
    elif i == 39:
        translateChar = translateChar + " "
        translateLowerChar = translateLowerChar + " "
    elif i == 34:
        translateChar = translateChar + " "
        translateLowerChar = translateLowerChar + " "
    elif i == 59:
        translateChar = translateChar + " "
        translateLowerChar = translateLowerChar + " "
    elif i == 40:
        translateChar = translateChar + " "
        translateLowerChar = translateLowerChar + " "
    elif i == 41:
        translateChar = translateChar + " "
        translateLowerChar = translateLowerChar + " "
    elif i == 123:
        translateChar = translateChar + " "
        translateLowerChar = translateLowerChar + " "
    elif i == 125:
        translateChar = translateChar + " "
        translateLowerChar = translateLowerChar + " "
    elif i == 60:
        translateChar = translateChar + " "
        translateLowerChar = translateLowerChar + " "
    elif i == 62:
        translateChar = translateChar + " "
        translateLowerChar = translateLowerChar + " "
    elif i == 61:
        translateChar = translateChar + " "
        translateLowerChar = translateLowerChar + " "
    elif i == 47:
        translateChar = translateChar + " "
        translateLowerChar = translateLowerChar + " "
    elif i == 42:
        translateChar = translateChar + " "
        translateLowerChar = translateLowerChar + " "
    elif i == 58:
        translateChar = translateChar + " "
        translateLowerChar = translateLowerChar + " "
    elif i == 35:
        translateChar = translateChar + " "
        translateLowerChar = translateLowerChar + " "
    elif i == 92:
        translateChar = translateChar + " "
        translateLowerChar = translateLowerChar + " "
    elif i == 10:
        translateChar = translateChar + " "
        translateLowerChar = translateLowerChar + " "
    elif i == 13:
        translateChar = translateChar + " "
        translateLowerChar = translateLowerChar + " "
    # TOLOWER
    elif i >= 65 and i <= 90:
        translateLowerChar = translateLowerChar + chr(i + 32)
        translateChar = translateChar + chr(i)
    else:
        translateLowerChar = translateLowerChar + chr(i)
        translateChar = translateChar + chr(i)

def cleanupString(str):
    return str.translate(translateChar)
    
def indexString(dictionnary, str):
    global translateChar

    if str == None or str == "":
	return
    try:
	norm = str.translate(translateLowerChar)
	for word in norm.split():
	    if len(word) < 3 or dropWords.has_key(word):
		continue
	    if dictionnary.has_key(word):
		dictionnary[word] = dictionnary[word] + 1
	    else:
		dictionnary[word] = 1
    except:
        pass
    
    
def run_unit_tests():
    pass

if __name__ == "__main__":
    run_unit_tests()


