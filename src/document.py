#!/usr/bin/env python
#
# document.py: front end to document opening and searching
#
#
# Copyright (C) 2005 Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

import os
import os.path
import util

def dsprint(fmt, *args):
    util.debug_print (util.DEBUG_SEARCH, fmt % args)

def dprint(fmt, *args):
    util.debug_print (util.DEBUG_DOCUMENT, fmt % args)

class DocumentException (Exception):
    """Exceptions raised by the Documents handlers"""

class Document:
    """Class abstracting a searchable document"""
    def __init__(self, URL):
        """The __init__ method should raise a Document exception id the
	   document is not of the expected type"""
	self.program = None
	pass

    def documentType(self):
        """A string identifying the document type"""
	return "undefined"

    def creationDate(self):
        """Date the document was created"""
	return "unknown"

    def creator(self):
        """who created the docuemnt"""
	return "unknown"

    def authors(self):
        """the list of all author names"""
	return ()

    def lastModification(self):
        """date of last modification"""
	return "unknown"

    def index(self):
        """return a weighted hash of the words in the documents."""
	return {}

    def set_program(self, path):
        self.program = path

    def get_program(self):
        return self.program

document_types = {}
def addDocumentType(suffixes, programs, docClass):
    for program in programs:
        path = util.get_exec_path(program)
	if path != None:
	    break

    for suffix in suffixes:
        dprint("Registering suffix %s:%s", suffix, path)
        document_types[suffix.lower()] = (docClass, path)


class DocumentStore:
    """This instance is a hub, registering all the type of document
       which can be analyzed and also keeping a collection of document
       informations"""
    def __init__(self, model = None, scanner = None):
        self.documents = {}
	self.model = model
	self.scanner = scanner

    def __addDocument(self, path, doc):
        dprint("registering document at %s", path)
        self.documents[path] = doc
	if self.model != None:
	    self.model.updateDocument(path, 0, doc.authors(),
	                              doc.lastModification(),
				      doc.documentType())
	self.model.updateCount()

    def __delDocument(self, path):
        dprint("unregistering document at %s", path)
        del self.documents[path]
	if self.model != None:
	    self.model.deleteDocument(path)
	self.model.updateCount()

    def printDocument(self, path):
        dprint("print for %s requested", path)

    def editDocument(self, path):
        dprint("edit for %s requested", path)
        try:
	    doc = self.documents[path]
	    program = doc.get_program()
	    if program != None :
	        os.spawnl(os.P_NOWAIT, program, program, path)
	except:
	    util.print_exception()


    def setScanner(self, scanner):
        self.scanner = scanner

    def setModel(self, model):
        self.model = scanner

    def getDocument(self, path):
        if self.documents.has_key(path):
	    return(self.documents[path])

    def remove(self, path):
        if not self.documents.has_key(path):
	    return
	self.__delDocument(path)

    def contains(self, path):
        if self.documents.has_key(path):
	    return 1
	dprint("store does not contain %s", path)
	return 0

    def analyze(self, path):
        path = os.path.abspath(path)
        for suffix in document_types.keys():
	    l = len(suffix)
	    if path[-l:].lower() == suffix:
	        try:
		    doc = document_types[suffix][0](path)
		    doc.set_program(document_types[suffix][1])
		    self.__addDocument(path, doc)
		    return(doc)
		except:
		    pass
	return None

		    
    def nbDocuments(self):
        return len(self.documents.keys())

    def locate(self):
        """Run l cate commands to find documents on the user tree"""
	dprint("Running locate commands")
	for suffix in document_types.keys():
	    try:
		res = os.popen3("locate %s | grep %s" %
				(suffix, util.get_home_dir()))
		lst = res[1].read().split()
		for file in lst:
		    try:
			if self.analyze(file) != None:
			    dir = os.path.dirname(file)
			    if self.scanner != None:
				self.scanner.addResource(dir)
	            except:
		        pass
	    except:
	        pass
	
    def search(self, query):
        """Return a list of document paths ordered by list of pertinence
	   to the query. Document not pertinent are just excluded."""

        if query == None or query == "" or self.nbDocuments() == 0:
	    return []
	dsprint("Searching for %s", query)
	words = query.split()
	res = []
	for path in self.documents.keys():
	    doc = self.documents[path]
	    dict = doc.index()
	    val = 0
	    for word in words:
	        try:
		    if dict.has_key(word):
		        dsprint("%s: %s : %d", path, word, dict[word])
			val = val * 10 + dict[word]
		except:
		    util.print_exception()
		    pass
	    if val != 0:
	        res.append((path, val))
	res.sort(lambda x, y: cmp(y[1], x[1]))
	ret = [ t[0] for t in res ]
	if ret == []:
	    dsprint("no hit for for %s", query)
	return ret

#
# import for the registration of the various back-ends
# 

import ooffice
