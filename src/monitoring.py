#!/usr/bin/env python
#
# monitoring.py: module to monitor files or directories
#                this is a simple wrapper around gamin Python wrappers
#
#
# Copyright (C) 2005 Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
import sys
import gobject
import gtk
import os.path
import util

try:
    import gamin
    with_gamin = 1

except:
    with_gamin = 0

def dprint(fmt, *args):
    util.debug_print (util.DEBUG_MONITOR, fmt % args)

_monitor = None
if with_gamin == 0:
    dprint("Gamin not found, monitoring desactivated")
else:
    try:
	_monitor = gamin.WatchMonitor()
	_monitor.no_exists()
	dprint("Connected to gamin monitor")

	def gaminWatch(source, condition):
	    dprint("gaminWatch called")
	    try:
		_monitor.handle_events()
	    except:
	        util.print_exception()
	    return gtk.TRUE

	_monitor_fd = _monitor.get_fd()
	_monitor_id = gobject.io_add_watch(_monitor_fd, 
	                                   gobject.IO_IN|gobject.IO_PRI,
					   gaminWatch)
	dprint("registered gamin monitor %d in I/O, id %s", _monitor_fd,
	       _monitor_id)
    except:
	dprint("Failed to connect to gamin, monitoring may not work")

class directoryMonitor:
    """monitor for a directory, give it a path, a callback function and
       an argument for said function, when files are added or removed
       callback(arg, path, val) will be called with val = 0 for deleted
       val = 1 for created and val = 2 for modified, and path will be a
       full path to the file."""
    def __init__(self, path, callback, arg):
        global _monitor

	if _monitor == None:
	    # TODO fallback to some basic polling
	    return
	self.path = os.path.abspath(path)
	self.callback = callback
	self.arg = arg
	_monitor.watch_directory(self.path, self.__gaminCallback)
        dprint("Added monitor for %s", self.path)

    def __gaminCallback(self, path, event):
        dprint("callback on %s: %s, %s", self.path, path, event)
	if event == gamin.GAMExists:
	    val = 1
	elif event == gamin.GAMDeleted:
	    val = 0
	elif event == gamin.GAMChanged:
	    val = 2
	else:
	    return
	try:
	    self.callback(self.arg, os.path.join(self.path, path), val)
	except:
	    util.print_exception()

class fileMonitor:
    """monitor for a file, give it a path, a callback function and
       an argument for said function, when files are added or removed
       callback(arg, path, val) will be called with val = 0 for deleted
       val = 1 for created and val = 2 for modified, and path will be a
       full path to the file."""
    def __init__(self, path, callback, arg):
        global _monitor

	if _monitor == None:
	    # TODO fallback to some basic polling
	    return
	self.path = os.path.abspath(path)
	self.callback = callback
	self.arg = arg
	_monitor.watch_file(self.path, self.__gaminCallback)
        dprint("Added monitor for file %s", self.path)

    def __gaminCallback(self, path, event):
        dprint("callback on file %s: %s, %s", self.path, path, event)
	if event == gamin.GAMExists:
	    val = 1
	elif event == gamin.GAMDeleted:
	    val = 0
	elif event == gamin.GAMChanged:
	    val = 2
	else:
	    return
	try:
	    self.callback(self.arg, self.path, val)
	except:
	    util.print_exception()
