#!/usr/bin/env python
#
# Implements the main part of the UI of Astrolabe
#
#
# Copyright (C) 2005 Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

import gtk
import gtk.glade
import gobject
import os.path
import util
import config
import indexing
import aboutdialog
import xmlconfig


ui = None

def dprint(fmt, *args):
    util.debug_print (util.DEBUG_UI, fmt % args)

class docmanChangesModel (gtk.ListStore):
    def __init__ (self, list, ui = None, store = None):
        # self is the drawed store, i.e. the subset of the full list of
	# documents indexed which match the subset
	# the full store has the original data of the information set
	# while self has a rendering presentation of it which may be
	# different.
        gtk.ListStore.__init__ (self, str, int, str, str, str)
	self.fullStore = {}
	self.__nb_items = 0
	self.__nb_display = 0
	self.__restricted = 0
	self.list = list
	self.order = [0, 0, 0, 0, 0]
	self.ui = ui
	self.store = store
	self.__sort = ""
        
    def setUI(self, ui):
        self.ui = ui

    def setStore(self, store):
        self.store = store

    def deleteDocument(self, filename):
        dprint("deleteDocument: %s", filename)
	if self.fullStore.has_key(filename):
	    del self.fullStore[filename]
	    i = 0
	    while i < self.__nb_display:
		file = self[i][0]
		if file == filename:
		    dprint("Found at row %d", i)
		    del self[i]
		    return
		i = i + 1
	else:
	    dprint("deleteDocument: %s not found !", filename)
	    

    def updateDocument(self, filename, size, author, date, mimetype = ""):
        dprint("updateDocument: %s %s %s", filename, author, date)
	if size == 0:
	    size = util.file_size(filename)
	if type(author) == type(()) or type(author) == type([]):
	    try:
		str = "%s" % (author[0])
		for auth in author[1:]:
		    str = str + ", %s" % auth
		author = str
	    except:
		author = "%s" % (author[0])

	try:
	    author = author.replace("  ", " ")
	    if author[0:2] == ', ':
	        author = author[2:]
	    author = author.replace('"', "")
	    author = author.replace('(', "")
	    author = author.replace(')', "")
	    author = author.replace('[', "")
	    author = author.replace(']', "")
	    author = author.replace("'", "")
	except:
	    util.print_exception()
	    pass
	if self.fullStore.has_key(filename):
	    # this is an update
	    is_new = 0
	else:
	    # this is a new document
	    self.__nb_items = self.__nb_items + 1
	    is_new = 1
	self.fullStore[filename] = (size, author, date, mimetype)

        if is_new:
	    if self.__restricted:
	        # best to requery and redraw
		dprint("updateDocument TODO")
	        pass
	    else:
	        # just add it at the end
		self.append([filename, size, author, date, mimetype])
		self.__nb_display = self.__nb_display + 1
	else:
	    # try to find it in the displayed list and update in-situ
	    i = 0
	    while i < self.__nb_display:
		file = self[i][0]
		if file == filename:
		    self[i] = [filename, size, author, date, mimetype]
		    return
		i = i + 1
        
    def sort(self, field):
        """Sort the displayed documents based on the field. This is
	   called by docmanUI.__xxxx_clicked() with the field number"""
	dprint("sorting on field %d , %d", field, self.order[field])
        i = 0
	values = []
	for r in self:
	    values.append((i, r[field]))
	    i = i + 1
	if self.order[field] == 0:
	    values.sort(lambda x, y: cmp(y[1], x[1]))
	    self.order[field] = 1
	else:
	    values.sort(lambda x, y: cmp(x[1], y[1]))
	    self.order[field] = 0
	new_order = [ r[0] for r in values ]
	self.reorder(new_order)
	
    def activate(self, no):
        """an entry has been activated"""
	dprint("activated %d", no)
	if self.store != None:
	    filename = self[no][0]
	    info = self.store.editDocument(filename)

    def updateCount(self):
        if self.ui != None:
	    self.ui.updateCount()

    def refresh(self):
        """refresh the drawing"""
	dprint("refresh called")
    
    def restrict(self, list, sort):
        """This is the call to restrict the rendering from the searc pattern.
	   The list is an order of matching document subset, if [] or None
	   this mean the display is not restricted anymore"""
	# TODO deactivate refresh on entry and reactivate refrash on exist
	dprint("restrict: %d, %s", len(list), sort)
	self.__sort = sort
        if list == None or list == []:
	    if self.__restricted != 0:
		self.__restricted = 0
		self.clear()
		i = 0
		for filename in self.fullStore.keys():
		    try:
			(size, author, date, type) = self.fullStore[filename]
			self.append([filename, size, author, date, type])
			i = i + 1
		    except:
		        util.print_exception()
		self.__nb_display = i
        else:
	    self.__restricted = 1
	    self.clear()
	    i = 0
	    for filename in list:
		try:
		    (size, author, date, type) = self.fullStore[filename]
		    self.append([filename, size, author, date, type])
		    i = i + 1
		except:
		    util.print_exception()
	    self.__nb_display = i

class docmanCell(gtk.CellRendererText):
    def __init__ (self, ui):
        gtk.CellRendererText.__init__(self)
	self.ui = ui

class docmanUI:
    """User interface for the document manager"""

    def __filename_renderer(self, column, cell, model, iter):
        filename = model.get_value(iter, 0)
	if len(filename) > 40:
	    cell.set_property('text', filename[-40:])

    def __init__ (self):
        global ui

	menu_callbacks = {
	    "on_add_activate": self.on_add_activate,
	    "on_quit_activate": self.on_quit_activate,
	    "on_search_button_activate": self.search,
	    "on_search_entry_editing_done": self.search,
	    "on_clear_button_activate": self.clear,
	    "on_about_activate": self.on_about_activate,
	    "on_open_activate": self.on_open_activate,
	    "on_add_location_activate": self.on_add_location_activate,
	    "on_refresh_activate": self.on_refresh_activate,
	    "on_copy_activate": self.on_copy_activate,
	    "on_paste_activate": self.on_paste_activate,
	    "on_edit_activate": self.on_edit_activate,
	    "on_view_activate": self.on_view_activate,
	    "on_print_activate": self.on_print_activate,
	    "on_execute_activate": self.on_execute_activate,
	    "on_preferences_activate": self.on_preferences_activate,
	    "on_sort_by_filename_activate": self.on_sort_by_filename_activate,
	    "on_sort_by_size_activate": self.on_sort_by_size_activate,
	    "on_sort_by_time_activate": self.on_sort_by_time_activate,
	    "on_sort_by_authors_activate": self.on_sort_by_authors_activate,
	    "on_sort_by_type_activate": self.on_sort_by_type_activate,
	    "on_select_all_activate": self.on_select_all_activate,
	    "on_search_activate": self.on_search_activate,
	    "on_help_activate": self.on_help_activate,
	    "on_add_location_cancel_activate": self.on_add_location_cancel_activate,
	    "on_add_location_ok_activate": self.on_add_location_ok_activate,
	}
	self.welcome = None

        self.glade_file = os.path.join (config.GLADEDIR, "astrolabe.glade")
        dprint("Initializing UI from %s", self.glade_file)
	self.xml = gtk.glade.XML (self.glade_file, "docman_dialog")
	self.dialog = self.xml.get_widget ("docman_dialog")
	self.statusbar = self.xml.get_widget ("statusbar")
	self.globalstatus = self.statusbar.get_context_id("global")
        self.statusbar.push(self.globalstatus, "Starting ...")
	appconfig = xmlconfig.get_config()
	if appconfig == None:
	    self.dialog.set_default_size (800, 600)
	else:
	    self.load_config(appconfig)
	
	self.dialog.connect ("destroy", gtk.main_quit)

	self.search_entry = self.xml.get_widget ("search_entry")
	self.search_history = gtk.ListStore(str)
	self.search_entry.set_model(self.search_history)
	cell = gtk.CellRendererText()
        self.search_entry.pack_start(cell, True)
	self.search_entry.add_attribute(cell, 'text', 0)  
	# self.search_entry.set_max_length(80)
	self.search_entry.child.connect("editing_done", self.search)
	self.search_entry.child.connect("activate", self.search)

	self.tree_view = self.xml.get_widget ("treeview")
	self.sel = self.tree_view.get_selection()
	self.sel.set_mode(gtk.SELECTION_SINGLE)
	self.sel.set_select_function(self.__row_selected, None)
	self.have_selection = False
	self.tree_view.selection_add_target("PRIMARY", "STRING", 1)
	self.tree_view.selection_add_target("PRIMARY", "COMPOUND_TEXT", 1)
	self.tree_view.connect("selection_get", self.selection_handle)

	self.model = docmanChangesModel(self.tree_view)
	self.model.setUI(self)
	self.tree_view.set_model(self.model)
	self.renderer = docmanCell(self)
	self.file_renderer = docmanCell(self)
	self.filename = gtk.TreeViewColumn("Filename", self.file_renderer,
	                                   text = 0)
	self.filename.set_cell_data_func(self.file_renderer, 
					 self.__filename_renderer)
	self.filename.connect("clicked", self.__filename_clicked)
	self.filename.set_max_width(300)
	self.filename.set_clickable(1)
	self.tree_view.append_column(self.filename)
	self.size = gtk.TreeViewColumn("Size", self.renderer,
	                                  text = 1)
	self.size.connect("clicked", self.__size_clicked)
	self.size.set_max_width(80)
	self.size.set_clickable(1)
	self.tree_view.append_column(self.size)
	self.authors = gtk.TreeViewColumn("Authors", self.renderer,
	                                  text = 2)
	self.authors.connect("clicked", self.__authors_clicked)
	self.authors.set_max_width(150)
	self.authors.set_clickable(1)
	self.tree_view.append_column(self.authors)
	self.modification = gtk.TreeViewColumn("Last change", self.renderer,
	                                       text = 3)
	self.modification.connect("clicked", self.__modification_clicked)
	self.modification.set_max_width(150)
	self.modification.set_clickable(1)

	self.tree_view.append_column(self.modification)
	self.type = gtk.TreeViewColumn("Document type", self.renderer,
				       text = 4)
	self.type.connect("clicked", self.__type_clicked)
	self.type.set_max_width(100)
	self.type.set_clickable(1)

	self.tree_view.append_column(self.type)
	self.xml.signal_autoconnect(menu_callbacks)

	self.dialog.show ()
	self.store = None
	self.scanner = None
	ui = self # dislike it but could not find simpler.

	if appconfig == None:
	    self.showWelcome()
        
    def selection_clear(self, widget, event):
        self.have_selection = False
	return True

    def selection_handle(self, widget, selection_data, info, time_stamp):
        if self.have_selection:
	    selection_data.set_text(self.selection_value,
	                        len(self.selection_value))
	return

    def __row_selected(self, path, user_data):
        dprint("__row_selected %d", path[0])
	try:
	    self.have_selection = self.tree_view.selection_owner_set("PRIMARY")
	    self.selection_value = self.model[path[0]][0]
	except:
	    self.selection_clear()
	return True

    def getModel(self):
        return self.model

    def setStore(self, store):
        self.store = store
	self.model.setStore(store)

    def setScanner(self, scanner):
        self.scanner = scanner

    def showWelcome(self, *arg):
        dprint("show Welcome")
	self.welcomexml = gtk.glade.XML (self.glade_file, "welcome")
	self.welcome = self.welcomexml.get_widget ("welcome")
	self.welcome.set_position(gtk.WIN_POS_MOUSE)
	self.welcome_label = self.welcomexml.get_widget ("welcome_label")
	self.welcome_label.set_text(_("""Welcome to Astrolabe

  This program will locate office document files
  found in your environment and allow to find
  the ones you need quickly using text search
  date of last changes or the authors."""))
        
	self.welcome_ok = self.welcomexml.get_widget ("welcome_ok_button")
	self.welcome_ok.connect("clicked", self.hideWelcome)
	self.welcome.show ()
	while gtk.events_pending() :
	    gtk.main_iteration(gtk.FALSE)

    def hideWelcome(self, *arg):
        dprint("hide Welcome")
        if self.welcome != None:
	    self.welcome.hide()
	    del self.welcome
	    self.welcome = None

    def clear(self, *arg):
        dprint("clear")
        self.search_entry.child.set_text("")
	self.model.restrict([], "")
	self.updateCount()
    
    def addSearchEntry(self, value):
        model = self.search_entry.get_model()
	i = 0
	while i < 20:
	    try:
		saved = model[i][0]
	    except:
	        saved = None
	        
	    if saved == value:
	        break
	    try:
		model[i][0] = value
	    except:
	        self.search_entry.append_text(value)
	    if saved == None:
	        break
	    value = saved
	    i = i + 1

    def search(self, *arg):
        active = self.search_entry.get_active()
	if active < 0:
	    value = self.search_entry.child.get_text().strip()
	    self.addSearchEntry(value)
	else:
	    model = self.search_entry.get_model()
	    value = model[active][0]
	if value == None or value == "":
	    self.clear()
	    return
        dprint("seach: %s", value)
	# LOWER
	lvalue = value.lower()
	if self.store == None:
	    return
	res = self.store.search(lvalue)
	if res == []:
	    status(_("No document found matching %s") % value)
	else:
	    status(_("Found %d documents matching %s") % (len(res), value))
	self.model.restrict(res, value)

    def on_add_activate(self, *arg):
        dprint("add")

    def on_quit_activate(self, *arg):
        dprint("quit")
        gtk.main_quit()

    def on_about_activate(self, *arg):
        dprint("about")
	aboutdialog.show_about_dialog (self.dialog)

    def on_open_activate(self, *arg):
        dprint("on_open_activate")

    def on_add_location_activate(self, *arg):
        dprint("on_add_location_activate")

    def on_refresh_activate(self, *arg):
        dprint("on_refresh_activate")
	if self.store != None:
	    status(_("Refreshing document list"))
	    while gtk.events_pending() :
		gtk.main_iteration(gtk.FALSE)
	    self.store.locate()

    def on_copy_activate(self, *arg):
        dprint("on_copy_activate")

    def on_paste_activate(self, *arg):
        dprint("on_paste_activate")

    def on_edit_activate(self, *arg):
        dprint("on_edit_activate")

    def on_view_activate(self, *arg):
        dprint("on_view_activate")

    def on_print_activate(self, *arg):
        dprint("on_print_activate")

    def on_execute_activate(self, *arg):
        dprint("on_execute_activate")

    def on_preferences_activate(self, *arg):
        dprint("on_preferences_activate")

    def on_sort_by_filename_activate(self, *arg):
        dprint("on_sort_by_filename_activate")
        self.model.sort(0)

    def on_sort_by_size_activate(self, *arg):
        dprint("on_sort_by_size_activate")
        self.model.sort(1)

    def on_sort_by_time_activate(self, *arg):
        dprint("on_sort_by_time_activate")
        self.model.sort(3)

    def on_sort_by_authors_activate(self, *arg):
        dprint("on_sort_by_authors_activate")
        self.model.sort(2)

    def on_sort_by_type_activate(self, *arg):
        dprint("on_sort_by_type_activate")
        self.model.sort(4)

    def on_select_all_activate(self, *arg):
        dprint("on_select_all_activate")
	self.clear()

    def on_search_activate(self, *arg):
        dprint("on_search_activate")
	self.search()

    def on_help_activate(self, *arg):
        dprint("on_help_activate")

    def on_add_location_cancel_activate(self, *arg):
        dprint("on_add_location_cancel_activate")

    def on_add_location_ok_activate(self, *arg):
        dprint("on_add_location_ok_activate")

    def exit(self):
        gtk.main_quit()

    def __filename_clicked(self, filename):
	self.model.sort(0)

    def __size_clicked(self, size):
	self.model.sort(1)

    def __authors_clicked(self, authors):
	self.model.sort(2)

    def __modification_clicked(self, modification):
	self.model.sort(3)

    def __type_clicked(self, modification):
	self.model.sort(4)

    def __row_activated(self, view, column, info):
        dprint("__row_activated")
        (model, iter) = self.sel.get_selected()
	if iter != None:
	    path = self.model.get_path(iter)
	    self.model.activate(path[0])

    def updateCount(self):
	try:
	    nbDocs = self.store.nbDocuments()
	    nbDirs = self.scanner.nbDirectories()
	    status(_("Found %d files in %d directories") % (nbDocs, nbDirs))
	except:
	    pass

    def load_config(self, doc):
        try:
	    size = doc.xpathEval("(//ui/size)")[0]
	    width = int(size.prop('width'))
	    height = int(size.prop('height'))
	    self.dialog.set_default_size(width, height)
	except:
	    dprint("failed to load size")
	    util.print_exception()
        try:
	    pos = doc.xpathEval("(//ui/position)")[0]
	    x = int(pos.prop('x'))
	    y = int(pos.prop('y'))
	    self.dialog.set_uposition(x, y)
	except:
	    dprint("failed to load position")
	    util.print_exception()
        
    def save_config(self, fd):
        fd.write("  <ui>\n")
	try:
	    (width, height) = self.dialog.get_size()
	    fd.write("    <size width='%d' height='%d'/>\n" % (width, height))
	except:
	    dprint("failed to save size")
	try:
	    (x, y) = self.dialog.get_position()
	    fd.write("    <position x='%d' y='%d'/>\n" % (x, y))
	except:
	    dprint("failed to save position")
        fd.write("  </ui>\n")

def statusGet(service):
    if ui != None:
        return ui.statusbar.get_context_id("service")
    
def statusPush(id, fmt, *args):
    if id == None or ui == None:
        return
    ui.statusbar.push(id, fmt % args)
    
def statusPop(id):
    if id == None or ui == None:
        return
    ui.statusbar.pop(id)

def status(fmt, *args):
    if ui != None:
	ui.statusbar.pop(ui.globalstatus)
	ui.statusbar.push(ui.globalstatus, fmt % args)
