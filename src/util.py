#!/usr/bin/env python
#
# Copyright (C) 2005 Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

import os
import os.path
import stat
import urlparse
import sys
import pwd
import gettext
import locale
import errno
from config import *

(
    DEBUG_SCANNER,
    DEBUG_DOCUMENT,
    DEBUG_UI,
    DEBUG_SEARCH,
    DEBUG_MONITOR,
    DEBUG_RECENT,
    DEBUG_CONFIG,
) = range (7)

debug_modules = {
    DEBUG_SCANNER       : ("scanner",        False),
    DEBUG_DOCUMENT      : ("document",       False),
    DEBUG_UI            : ("ui",             False),
    DEBUG_SEARCH        : ("search",         False),
    DEBUG_MONITOR       : ("monitor",        False),
    DEBUG_RECENT        : ("recent",         False),
    DEBUG_CONFIG        : ("config",         False),
}

def init_debug_modules ():
    debug_value = os.getenv ("ASTROLABE_DEBUG")
    if not debug_value:
        return

    if debug_value == "help":
        print "Valid options for the ASTROLABE_DEBUG environment variable are:\n"
        print "    all"
        for module in debug_modules:
            print "    %s" % debug_modules[module][0]
        print "You may supply a list of modules separated by a colon (:)"
        print "You may also supply an optional hex debug mask to a module, e.g. foo=0xF8"
        sys.exit (1)
    elif debug_value == "all":
        for module in debug_modules:
            debug_modules[module] = (debug_modules[module][0], True)
    else:
        for item in debug_value.split (":"):
            item = item.split("=")
            key = item[0]
            if len(item) > 1:
                value = int(item[1],16)
            else:
                value = ~0
            for module in debug_modules:
                if debug_modules[module][0] == key:
                    debug_modules[module] = (key, value)
                    break

init_debug_modules ()

def debug_print (module, message, mask=~0):
    assert debug_modules.has_key(module)
    if not debug_modules[module][1] & mask:
        return
    print "(%d) %s: %s" % (os.getpid (), debug_modules[module][0], message)

class GeneralError (Exception):
    def __init__ (self, msg):
        Exception.__init__ (self, msg)

def file_size(path):
    try:
	info = os.stat(path)
    except:
        return -1
    return info[6]
    
def is_executable(path):
    try:
	info = os.stat(path)
    except:
        return 0
    if stat.S_ISDIR(info[0]):
	return 0
    else:
	return os.access(path, os.X_OK)

def is_readable(path):
    try:
	info = os.stat(path)
    except:
        return 0
    if stat.S_ISDIR(info[0]):
	return os.access(path, os.R_OK | os.X_OK)
    else:
	return os.access(path, os.R_OK)

def is_directory(path):
    try:
	info = os.stat(path)
    except:
        return 0
    return stat.S_ISDIR(info[0])

def is_remote(URL):
    try:
	decomp = urlparse.urlparse(URL)
	if decomp[2] == URL:
	    return 0
	return 1
    except:
        pass
    return 0

def get_home_dir ():
    try:
        pw = pwd.getpwuid (os.getuid ())
        if pw.pw_dir != "":
            return pw.pw_dir
    except KeyError:
        pass
    
    if os.environ.has_key ("HOME"):
        return os.environ["HOME"]
    else:
        raise GeneralError (_("Cannot find home directory: not set in /etc/passwd and no value for $HOME in environment"))

def get_user_name ():
    try:
        pw = pwd.getpwuid (os.getuid ())
        if pw.pw_name != "":
            return pw.pw_name
    except KeyError:
        pass
    
    if os.environ.has_key ("USER"):
        return os.environ["USER"]
    else:
        raise GeneralError (_("Cannot find username: not set in /etc/passwd and no value for $USER in environment"))

def get_exec_path (name):
    if os.environ.has_key ("PATH"):
        path = os.environ["PATH"]
    else:
	path = "/usr/local/bin:/usr/bin:/usr/bin/X11:/bin:/opt/bin"
    dirs = path.split(":")
    for dir in dirs:
        try:
	    path = dir + "/" + name
	    if is_executable(path):
		return(path)
	except:
	    pass
    return None

def print_exception ():
    import traceback
    import sys
    traceback.print_exc(file=sys.stderr)

def init_gettext ():
    """Binds _() to gettext.gettext() in the global namespace. Run
    util.init_gettext() at the entry point to any script and you'll
    be able to use _() to mark strings for translation.
    """
    locale.setlocale (locale.LC_ALL, "")
    gettext.install (PACKAGE, os.path.join (DATADIR, "locale"))

