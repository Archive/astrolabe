#!/usr/bin/env python
#
# recent.py: handling of the .recently-used
#
#
# Copyright (C) 2005 Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
import os
import os.path
import libxml2
import util
import monitoring

def dprint(fmt, *args):
    util.debug_print (util.DEBUG_RECENT, fmt % args)

class recentFiles:
    def __init__(self, scanner, path=None):
        self.scanner = scanner
	dprint("checking recent files")
	if path == None:
	    path = os.path.join(util.get_home_dir(), ".recently-used")
	self.path = path
	self.__checkRecents()
	self.__monitor = monitoring.fileMonitor(self.path,
	                                 self.__changeCallback, None)

    def __changeCallback(self, arg, path, val):
        dprint("Gor change callback %d on %s", val, path)
	self.__checkRecents()

    def __checkRecents(self):
	dprint("checking file %s", self.path)
	try:
	    doc = libxml2.readFile(self.path, None, 0)
	except:
	    return
	uris = doc.xpathEval("//URI")
	for uri in uris:
	    self.__check_uri(uri.content)
	doc.freeDoc()


    def __check_uri(self, URI):
        dprint("checking found URI %s", URI)
        if URI[0:8] == "file:///":
	    URI = URI[8:]
        elif URI[0:17] == "file://localhost/":
	    URI = URI[17:]
	if util.is_remote(URI):
	    return
	try:
	    dir = os.path.dirname(URI)
	    self.scanner.addResource(dir)
	except:
	    pass

    
def run_unit_tests():
    pass

if __name__ == "__main__":
    run_unit_tests()


