#!/usr/bin/env python
#
# The scanning part locating documents on the hard drive.
#
#
# Copyright (C) 2005 Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

import sys
import os.path
import util
import monitoring
import libxml2

def dprint(fmt, *args):
    util.debug_print (util.DEBUG_SCANNER, fmt % args)

class DocumentScanner:
    """Implement a document scanner, checking a list of directories
       and also notifying the application of document changes"""

    def __init__(self, store = None, documentStore = None):
        dprint("Initializing the document scanner")
        self.store = store
	self.document_store = documentStore
	self.directories = {}

    def __updateDocument(self, filename, author, date, type):
        if self.store != None:
	    self.store.updateDocument(filename, 0, author, date, type)

    def __analyze(self, filename):
        if not util.is_readable(filename):
	    dprint("Resource not readable : %s", filename)
	    return
	if util.is_directory(filename):
	    return # recursing might be too expensive
	if self.document_store == None:
	    dprint("No document store to process %s", filename)
	    return
	doc = self.document_store.analyze(filename)

    def __directoryUpdate(self, URL, path, change):
        dprint("directory %s: update %s %d", URL, path, change)
	if self.document_store == None:
	    return
	if URL == path:
	    if change == 0:
	        dprint("TODO: directory %s removed", URL)
		try:
		    del self.directories[URL]
		except:
		    pass
	    elif change == 2:
	        self.addResource(URL)
	else:
	    if self.document_store.contains(path):
	        if change == 0:
		    self.document_store.remove(path)
		else:
		    self.__analyze(path)
	    else:
	        # if the file is not watched, check only new files
	        if change == 2:
		   self.__analyze(path)

    def addResource(self, URL):
        if util.is_remote(URL):
	    dprint("remote access not supported yet: %s", URL)
	    return
	if not util.is_readable(URL):
	    dprint("Resource not readable : %s", URL)
	URL = os.path.abspath(URL)
	if util.is_directory(URL):
	    if self.directories.has_key(URL):
	        dprint("Directory %s already scanned")
		return
	    try:
		self.directories[URL] = monitoring.directoryMonitor(URL,
						  self.__directoryUpdate, URL)
	    except:
	        util.print_exception
	    for file in os.listdir(URL):
	        self.__analyze(URL + '/' + file)
	else:
	    self.__analyze(URL)
	    
    def nbDirectories(self):
        return(len(self.directories.keys()))
         
    def load_config(self, doc):
        dirs = doc.xpathEval("//scanner/directory")
	for dir in dirs:
	    try:
	        path = dirs.prop("path")
		self.addResource(path)
	    except:
	        pass
	    
        
    def save_config(self, fd):
        fd.write("  <scanner>\n")
	for dir in self.directories.keys():
	    # TODO escape the filenames to not clash with the UTF-8 encoding
	    fd.write("    <directory path='%s'/>\n" % (dir))
        fd.write("  </scanner>\n")

def run_unit_tests():
    pass

if __name__ == "__main__":
    run_unit_tests()


